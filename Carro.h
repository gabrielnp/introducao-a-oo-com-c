#ifndef CARRO_H_
#define CARRO_H_
#include <string>

using namespace std;


class Carro{
	public:
		static int pneus;
		Carro(); // método construtor Carro
		Carro(string marca, int ano);
		~Carro(); // método destrutor Carro
		void setMarca(string marca);
		string getMarca();
		void setAno(int ano);
		int getAno();
	private:
		string marca;
		int ano;
};

#endif /* CARRO_H_ */
