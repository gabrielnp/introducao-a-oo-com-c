#include "Carro.h"
#include <string>
#include <iostream>

using namespace std;

void Carro::setMarca(string marca){
	this->marca = marca;
}
string Carro::getMarca(){
	return marca;
}

void Carro::setAno(int ano){
	if(ano > 1990)
		this->ano = ano;
	else
		this->ano = 1990;
}
int Carro::getAno(){
	return ano;
}

Carro::Carro(){
	this->marca="Marca não preenchida";
}

Carro::Carro(string marca, int ano){
	this->marca=marca;
	if(ano > 1990)
		this->ano=ano;
	else
		this->ano=1990;
}

Carro::~Carro(){
	cout << "Carro foi destruído!" << endl;
}

int Carro::pneus = 4;
