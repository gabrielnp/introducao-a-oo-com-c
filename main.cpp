#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

#include "Carro.h"

int main (int argc, char** argv){

	Carro c1;
	c1.setMarca("Fiat");
	c1.setAno(2017);
	c1.pneus = 2;

	Carro c2("Ferrari",1650);
	c2.setMarca("Chevrolet");
	c2.setAno(1700);

	cout << c1.getMarca() << endl;
	cout << c1.getAno() << endl;
	cout << c1.pneus << endl;

	cout << c2.getMarca() << endl;
	cout << c2.getAno()<< endl;
	cout << c2.pneus << endl;
	return 0;
}

